# Flips solver library

The Flips solver solves classical and augmented chemical reaction networks, as described in the publication

*Beyond the chemical master equation: Stochastic chemical kinetics coupled with auxiliary processes.*  
Lunz D, Batt G, Ruess J, Bonnans JF (2021)  
PLOS Computational Biology 17(7): e1009214.  
https://doi.org/10.1371/journal.pcbi.1009214

## Installation

The package is installed by running:

    pip install flips

## Tutorial

A simple introductory tutorial is available in the Tutorial IPython notebook.