import flips
import unittest
import numpy as np
from unittest.mock import patch

flips.solver.verbose = 0 # global no printing

class TestFlips(unittest.TestCase):

    def test_inputs(self):
        b = d = 1
        with self.assertRaises(Exception):
            flips.solver([
                [{}, {'X':0}, b],
                [{'X':1}, {}, d],
            ])
        with self.assertRaises(Exception):
            flips.solver([
                [{}, {2:1}, b],
                [{'X':1}, {}, d],
            ])
        with self.assertRaises(Exception):
            flips.solver([
                [{}, {'X':1.0}, b],
                [{'X':1}, {}, d],
            ])
        with self.assertRaises(Exception):
            flips.solver([
                [{}, {'X':[1]}, b],
                [{'X':1}, {}, d],
            ])
        with self.assertRaises(Exception):
            flips.solver([
                [{}, {'X':-1}, b],
                [{'X':1}, {}, d],
            ])
        with self.assertWarns(UserWarning):
            flips.solver([
                [{}, {'X':2}, b],
            ])
        
    @patch("flips.plt.figure")
    def test_single(self, mock_show):
        b = d = 1
        o = flips.solver([
            [{}, {'X':1}, b],
            [{'X':1}, {}, d],
        ], num_lumps=10)
        self.assertEqual(o.crn.species, ['X'])
        self.assertEqual(o.crn.num_species, 1)
        self.assertEqual(o.crn.discrete_species, [])
        self.assertEqual(o.crn.num_discrete_species, 0)
        o.set_initial_conditions(1)
        o.solve(5, bar='console')
        o.plot_p()
        o.plot_mean()
        o.plot_sigma()
        
        b = d = 1
        X_rctn_rate = lambda S,L,t:d*S['X']*(S['X']-1/L)/2
        X_rctn_rate.t = False
        
        o = flips.solver([
            [{}, {'X':1}, b],
            [{'X':1}, {}, X_rctn_rate],
        ], num_lumps=10)
        self.assertEqual(o.crn.species, ['X'])
        self.assertEqual(o.crn.num_species, 1)
        self.assertEqual(o.crn.discrete_species, [])
        self.assertEqual(o.crn.num_discrete_species, 0)
        o.set_initial_conditions(0)
        o.solve(5, bar='console')
        o.plot_p()
        o.plot_mean()
        o.plot_sigma()
        
        a = b = d = 1
        o = flips.solver([
            [{}, {'X':1}, flips.burst(a,b)],
            [{'X':1}, {}, d],
        ], num_lumps=10)
        self.assertEqual(o.crn.species, ['X'])
        self.assertEqual(o.crn.num_species, 1)
        self.assertEqual(o.crn.discrete_species, [])
        self.assertEqual(o.crn.num_discrete_species, 0)
        o.set_initial_conditions(-1)
        o.solve(5, bar='console')
        o.plot_p()
        o.plot_mean()
        o.plot_sigma()
        
    @patch("flips.plt.figure")
    def test_multiple(self, mock_show):
        b = 3
        d = 1
        fn = lambda S,L,t: b*S['X']*(1-S['X'])
        fn.t = False
        o = flips.solver([
            [{}, {'X':1}, fn],
            [{'X':1}, {'Y':1}, d],
            [{'X':1}, {}, d],
            [{'Y':1}, {'X':1}, d],
        ], num_lumps=10)
        self.assertEqual(o.crn.species, ['X','Y'])
        o.set_initial_conditions(-1)
        o.solve(5, bar='console')
        o.plot_p()
        o.plot_p('X')
        
        o = flips.solver([
            [{}, {'X':1}, fn],
            [{'X':1}, {'Y':1}, d],
            [{'X':1}, {}, d],
            [{'Y':1}, {'Z':1}, d],
            [{'Z':1}, {}, d],
        ], num_lumps=10)
        self.assertEqual(o.crn.species, ['X','Y','Z'])
        with self.assertRaises(Exception):
            o.plot_sigma()
        o.set_initial_conditions(-1)
        o.solve(5, bar='console')
        o.plot_p('Z')
        o.plot_p(['X','Z'])
    
    @patch("flips.plt.figure")
    def test_discrete(self, mock_show):
        M = 3
        N = 2
        a = b = d = delta = 1
        o = flips.solver([
            [{}, {'X':1}, lambda m:b*m['m']],
            [{'X':1}, {}, d],
        ], discrete_truncs={'m':M}, discrete_reactions=[
            [{}, {'m':N}, a],
            [{'m':1}, {}, lambda m:delta*m['m']]
        ], num_lumps=10)
    
        self.assertEqual(o.crn.species, ['X'])
        for i in range(0,M-N):
            self.assertNotEqual(o.crn.discrete_reactions[0].get_rate((i,)), 0)
        for i in range(M-N,M):
            self.assertEqual(o.crn.discrete_reactions[0].get_rate((i,)), 0)
        for i in range(0,1):
            self.assertEqual(o.crn.discrete_reactions[1].get_rate((i,)), 0)
        for i in range(1,M):
            self.assertNotEqual(o.crn.discrete_reactions[1].get_rate((i,)), 0)
        
        o.set_initial_conditions(-1)
        o.solve(5, bar='console')
        o.plot_p()
        o.plot_mean()
        o.plot_sigma()
        
    @patch("flips.plt.figure")
    def test_multiple_discrete(self, mock_show):
        crn = get_multiple_discrete()
        crn.set_initial_conditions(-1)
        crn.solve(5, bar='console')
        crn.plot_p(['X','Y'])
        crn.plot_p(['X'])
        crn.plot_mean(['X'])
        crn.plot_sigma(['Y'])
        with self.assertRaises(Exception):
            crn.plot_sigma(['Z'])
    
    def test_ics(self):
        crn = get_multiple_discrete()
        
        crn.set_initial_conditions(np.random.rand(*crn.get_zero_initial_conditions().shape))
        icsr = crn.p
        crn.set_initial_conditions(1)
        ics1 = crn.p
        crn.set_initial_conditions(0)
        ics0 = crn.p
        crn.set_initial_conditions(-1)
        icsm1 = crn.p
        self.assertAlmostEqual(icsr.sum(), icsm1.sum())
        self.assertAlmostEqual(icsr.sum(), ics1.sum())
        self.assertAlmostEqual(icsr.sum(), ics0.sum())
        with self.assertRaises(Exception):
            crn.set_initial_conditions(-icsr)
            
    @patch("flips.plt.figure")
    def test_solvers(self, mock_show):
        def solve_scheme(scheme):
            b = d = 1
            o = flips.solver([
                [{}, {'X':1}, b],
                [{'X':1}, {}, d],
            ], num_lumps=10, scheme=scheme, dt=1e-1)
            o.set_initial_conditions(0)
            o.solve(5, bar='console')
            o.plot_p()
            o.plot_mean()
            o.plot_sigma()
        # there will be a warning of an invalid value in the sqrt, because the high-order schemes are not
        # monotonic so obtain negative values. This is unavoidable
        for scheme in flips.solver.methods.keys():
            if scheme == 'third order':
                with self.assertWarns(RuntimeWarning):
                    solve_scheme(scheme)
            else:
                solve_scheme(scheme)
            
    def test_steppers(self):
        for stepper in ['RK', 'multilevel']:
            b = d = 1
            o = flips.solver([
                [{}, {'X':1}, b],
                [{'X':1}, {'Y':1}, d],
                [{'X':1}, {}, d],
                [{'Y':1}, {'X':1}, d],
            ], num_lumps=10, stepper=stepper, step_order=2, step_level=3)
            o.set_initial_conditions(0)
            o.solve(5, bar='console')        
    
def get_multiple_discrete():
    M = 3
    N = 1
    a = b = d = delta = 1
    crn = flips.solver([
        [{}, {'X':1}, lambda m:b*m['m']],
        [{'X':1}, {'Y':1}, d],
        [{'X':1}, {}, d],
        [{'Y':1}, {'X':1}, d],
    ], discrete_truncs={'m':M}, discrete_reactions=[
        [{}, {'m':N}, a],
        [{'m':1}, {}, lambda m:delta*m['m']]
    ], num_lumps=10)
    return crn
    
if __name__ == '__main__':
    unittest.main()